/*
 ************************************************************************************
 * Copyright (C) 2001-2022 Openbravo S.L.U.
 * Licensed under the Apache Software License version 2.0
 * You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to  in writing,  software  distributed
 * under the License is distributed  on  an  "AS IS"  BASIS,  WITHOUT  WARRANTIES  OR
 * CONDITIONS OF ANY KIND, either  express  or  implied.  See  the  License  for  the
 * specific language governing permissions and limitations under the License.
 ************************************************************************************
 */

package org.openbravo.ddlutils.task;

import java.io.File;

import org.openbravo.ddlutils.process.DBCreator;

/**
 * 
 * @author Adrian
 */
public class CreateDatabase extends BaseDatabaseTask {
  private File prescript = null;
  private File postscript = null;

  private File model;
  private boolean dropfirst = false;
  private boolean failonerror = false;

  private String object = null;

  private String basedir;
  private String modulesDir;

  private String dirFilter;
  private String input;

  @Override
  public void doExecute() {
    getLog().info("Database connection: " + getUrl() + ". User: " + getUser() + ". System User: "
        + getSystemUser());
    DBCreator dbCreator = getDBCreator();
    initDBCreator(dbCreator);
    dbCreator.createDatabase();
  }

  protected DBCreator getDBCreator() {
    return new DBCreator();
  }

  protected void initDBCreator(DBCreator dbCreator) {
    dbCreator.setModel(model)
        .setBasedir(basedir)
        .setDirFilter(dirFilter)
        .setDropfirst(dropfirst)
        .setFailonerror(failonerror)
        .setPrescript(prescript)
        .setPostscript(postscript)
        .setObject(object)
        .setInput(input)
        .setModulesDir(modulesDir)
        .setPlatform(getPlatformInstance());
  }

  public File getModel() {
    return model;
  }

  public void setModel(File model) {
    this.model = model;
  }

  public String getBasedir() {
    return basedir;
  }

  public void setBasedir(String basedir) {
    this.basedir = basedir;
  }

  public String getDirFilter() {
    return dirFilter;
  }

  public void setDirFilter(String dirFilter) {
    this.dirFilter = dirFilter;
  }

  public boolean isDropfirst() {
    return dropfirst;
  }

  public void setDropfirst(boolean dropfirst) {
    this.dropfirst = dropfirst;
  }

  public boolean isFailonerror() {
    return failonerror;
  }

  public void setFailonerror(boolean failonerror) {
    this.failonerror = failonerror;
  }

  public File getPrescript() {
    return prescript;
  }

  public void setPrescript(File prescript) {
    this.prescript = prescript;
  }

  public File getPostscript() {
    return postscript;
  }

  public void setPostscript(File postscript) {
    this.postscript = postscript;
  }

  public void setObject(String object) {
    if (object == null || object.trim().startsWith("$") || object.trim().equals("")) {
      this.object = null;
    } else {
      this.object = object;
    }
  }

  public String getObject() {
    return object;
  }

  public String getInput() {
    return input;
  }

  public void setInput(String input) {
    this.input = input;
  }

  public String getModulesDir() {
    return modulesDir;
  }

  public void setModulesDir(String modulesDir) {
    this.modulesDir = modulesDir;
  }
}
