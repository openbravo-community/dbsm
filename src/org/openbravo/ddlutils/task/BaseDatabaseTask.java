/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.1  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html 
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License. 
 * The Original Code is Openbravo ERP. 
 * The Initial Developer of the Original Code is Openbravo SLU 
 * All portions are Copyright (C) 2009-2022 Openbravo SLU
 * All Rights Reserved. 
 * Contributor(s):  ______________________________________.
 ************************************************************************
 */

package org.openbravo.ddlutils.task;

import org.apache.commons.dbcp.BasicDataSource;
import org.apache.ddlutils.Platform;
import org.apache.ddlutils.PlatformFactory;
import org.apache.log4j.Logger;
import org.apache.tools.ant.Task;
import org.openbravo.ddlutils.util.DBSMOBUtil;

/**
 * This is the base class for the database ant tasks. It provides logging and other base
 * functionality.
 * 
 * @author mtaal
 */
public abstract class BaseDatabaseTask extends Task {
  private String driver;
  private String url;
  private String user;
  private String password;
  private String systemUser;
  private String systemPassword;
  private int threads = 0;

  protected Logger log;

  /**
   * Initializes the logging.
   */
  protected void initLogging() {
    log = Logger.getLogger(getClass());
  }

  @Override
  public void execute() {
    initLogging();
    doExecute();
  }

  // Needs to be implemented subclass, does the real execute
  protected abstract void doExecute();

  public Logger getLog() {
    if (log == null) {
      initLogging();
    }
    return log;
  }

  public String getDriver() {
    return driver;
  }

  public void setDriver(String driver) {
    this.driver = driver;
  }

  public String getUrl() {
    return url;
  }

  public void setUrl(String url) {
    this.url = url;
  }

  public String getUser() {
    return user;
  }

  public void setUser(String user) {
    this.user = user;
  }

  public String getPassword() {
    return password;
  }

  public void setPassword(String password) {
    this.password = password;
  }

  public String getSystemUser() {
    return systemUser;
  }

  public void setSystemUser(String systemUser) {
    this.systemUser = systemUser;
  }

  public String getSystemPassword() {
    return systemPassword;
  }

  public void setSystemPassword(String systemPassword) {
    this.systemPassword = systemPassword;
  }

  public void setLog(Logger log) {
    this.log = log;
  }

  /** Defines how many threads can be used to execute parallelizable tasks */
  public void setThreads(int threads) {
    this.threads = threads;
  }

  protected Platform getPlatformInstance() {
    BasicDataSource ds = DBSMOBUtil.getDataSource(getDriver(), getUrl(), getUser(), getPassword());
    Platform platform = PlatformFactory.createNewPlatformInstance(ds);
    if (getSystemUser() != null && getSystemPassword() != null) {
      // Create the data source used to execute statements with the system user
      BasicDataSource systemds = DBSMOBUtil.getDataSource(getDriver(), getUrl(), getSystemUser(),
          getSystemPassword());
      platform.setSystemDataSource(systemds);
    }
    platform.setMaxThreads(threads);

    return platform;
  }
}
