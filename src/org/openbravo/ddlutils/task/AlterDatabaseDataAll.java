/*
 ************************************************************************************
 * Copyright (C) 2001-2022 Openbravo S.L.U.
 * Licensed under the Apache Software License version 2.0
 * You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to  in writing,  software  distributed
 * under the License is distributed  on  an  "AS IS"  BASIS,  WITHOUT  WARRANTIES  OR
 * CONDITIONS OF ANY KIND, either  express  or  implied.  See  the  License  for  the
 * specific language governing permissions and limitations under the License.
 ************************************************************************************
 */

package org.openbravo.ddlutils.task;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

import org.apache.commons.lang.StringUtils;
import org.apache.ddlutils.Platform;
import org.apache.ddlutils.platform.ExcludeFilter;
import org.openbravo.ddlutils.process.DBUpdater;
import org.openbravo.ddlutils.util.DBSMOBUtil;

/**
 * 
 * @author adrian
 */
public class AlterDatabaseDataAll extends BaseDatabaseTask {

  protected File input;
  protected String encoding = "UTF-8";
  protected File originalmodel;
  protected File model;
  protected boolean failonerror = false;

  protected String object = null;

  protected String basedir;
  protected String dirFilter;
  protected String datadir;
  protected String datafilter;
  protected boolean force = false;
  protected boolean onlyIfModified = false;
  protected boolean strict;

  protected ExcludeFilter excludeFilter;

  private String forcedRecreation = "";

  private boolean executeModuleScripts = true;

  private DBUpdater dbUpdater;

  @Override
  protected void doExecute() {
    DBUpdater updater = getDBUpdater();
    updater.update();
  }

  protected DBUpdater getDBUpdater() {
    if (dbUpdater == null) {
      getLog().info("Database connection: " + getUrl() + ". User: " + getUser() + ". System User: "
          + getSystemUser());
      dbUpdater = new DBUpdater().setLog(getLog())
          .setExcludeFilter(DBSMOBUtil.getInstance()
              .getExcludeFilter(new File(model.getAbsolutePath() + "/../../../")))
          .setPlatform(getPlatformInstance())
          .setModel(model)
          .setBasedir(basedir)
          .setStrict(strict)
          .setFailonerror(failonerror)
          .setForce(force)
          .setExecuteModuleScripts(executeModuleScripts)
          .setDatafilter(datafilter)
          .setBaseSrcAD(input)
          .setDirFilter(dirFilter)
          .setDatasetName("AD")
          .setCheckDBModified(true)
          .setCheckFormalChanges(true)
          .setUpdateModuleInstallTables(true);
    }
    return dbUpdater;
  }

  @Override
  protected Platform getPlatformInstance() {
    Platform platform = super.getPlatformInstance();

    if (!StringUtils.isEmpty(forcedRecreation)) {
      getLog().info("Forced recreation: " + forcedRecreation);
    }
    platform.getSqlBuilder().setForcedRecreation(forcedRecreation);
    return platform;
  }

  public static void main(String[] args) throws IOException {
    AlterDatabaseDataAll task = new AlterDatabaseDataAll();

    String workingDir = System.getProperty("user.dir");
    task.getLog().info("Working directory: " + workingDir);

    Properties props = new Properties();
    try (FileInputStream fis = new FileInputStream(workingDir + "/config/Openbravo.properties")) {
      props.load(fis);
    }
    String baseDir = workingDir + "/src-db/database";

    task.setDriver(props.getProperty("bbdd.driver"));

    String ownerUrl;
    if ("POSTGRE".equals(props.getProperty("bbdd.rdbms"))) {
      ownerUrl = props.getProperty("bbdd.url") + "/" + props.getProperty("bbdd.sid");
    } else {
      ownerUrl = props.getProperty("bbdd.url");
    }

    String urlParams = props.getProperty("bbdd.props");
    if (StringUtils.isNotBlank(urlParams)) {
      ownerUrl += "?" + urlParams;
    }

    task.setUrl(ownerUrl);
    task.setUser(props.getProperty("bbdd.user"));
    task.setPassword(props.getProperty("bbdd.password"));
    task.setSystemUser(props.getProperty("bbdd.systemUser"));
    task.setSystemPassword(props.getProperty("bbdd.systemPassword"));
    task.setModel(new File(baseDir + "/model"));

    task.setInput(new File(baseDir + "/sourcedata"));
    task.setObject(props.getProperty("bbdd.object"));
    task.setBasedir(workingDir + "/modules");
    task.setDirFilter("*/src-db/database/model");
    task.setDatadir(workingDir + "/modules");
    task.setDatafilter("*/src-db/database/sourcedata");
    task.setForce(false);
    task.setFailonerror(false);

    task.execute();
  }

  public File getOriginalmodel() {
    return originalmodel;
  }

  public void setOriginalmodel(File input) {
    this.originalmodel = input;
  }

  public File getModel() {
    return model;
  }

  public void setModel(File model) {
    this.model = model;
  }

  public String getBasedir() {
    return basedir;
  }

  public void setBasedir(String basedir) {
    this.basedir = basedir;
  }

  public String getDirFilter() {
    return dirFilter;
  }

  public void setDirFilter(String dirFilter) {
    this.dirFilter = dirFilter;
  }

  public boolean isFailonerror() {
    return failonerror;
  }

  public void setFailonerror(boolean failonerror) {
    this.failonerror = failonerror;
  }

  public File getInput() {
    return input;
  }

  public void setInput(File input) {
    this.input = input;
  }

  public String getEncoding() {
    return encoding;
  }

  public void setEncoding(String encoding) {
    this.encoding = encoding;
  }

  public void setObject(String object) {
    if (object == null || object.trim().startsWith("$") || object.trim().equals("")) {
      this.object = null;
    } else {
      this.object = object;
    }
  }

  public String getObject() {
    return object;
  }

  public String getDatadir() {
    return datadir;
  }

  public void setDatadir(String datadir) {
    this.datadir = datadir;
  }

  public String getDatafilter() {
    return datafilter;
  }

  public void setDatafilter(String datafilter) {
    this.datafilter = datafilter;
  }

  public boolean isForce() {
    return force;
  }

  public void setForce(boolean force) {
    this.force = force;
  }

  public boolean isOnlyIfModified() {
    return onlyIfModified;
  }

  public void setOnlyIfModified(boolean onlyIfModified) {
    this.onlyIfModified = onlyIfModified;
  }

  public boolean isStrict() {
    return strict;
  }

  public void setStrict(boolean strict) {
    this.strict = strict;
  }

  public void setForcedRecreation(String forcedRecreation) {
    this.forcedRecreation = forcedRecreation;
  }

  public void setExecuteModuleScripts(boolean executeModuleScripts) {
    this.executeModuleScripts = executeModuleScripts;
  }

}
