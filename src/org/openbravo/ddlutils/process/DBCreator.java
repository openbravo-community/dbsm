/*
 ************************************************************************************
 * Copyright (C) 2022 Openbravo S.L.U.
 * Licensed under the Apache Software License version 2.0
 * You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to  in writing,  software  distributed
 * under the License is distributed  on  an  "AS IS"  BASIS,  WITHOUT  WARRANTIES  OR
 * CONDITIONS OF ANY KIND, either  express  or  implied.  See  the  License  for  the
 * specific language governing permissions and limitations under the License.
 ************************************************************************************
 */

package org.openbravo.ddlutils.process;

import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;
import java.util.Vector;
import java.util.stream.Collectors;

import org.apache.ddlutils.Platform;
import org.apache.ddlutils.alteration.Change;
import org.apache.ddlutils.alteration.DataChange;
import org.apache.ddlutils.alteration.ModelChange;
import org.apache.ddlutils.io.DataReader;
import org.apache.ddlutils.io.DatabaseDataIO;
import org.apache.ddlutils.io.DatabaseIO;
import org.apache.ddlutils.model.Database;
import org.apache.ddlutils.model.DatabaseData;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.tools.ant.BuildException;
import org.apache.tools.ant.DirectoryScanner;
import org.openbravo.ddlutils.task.DatabaseUtils;
import org.openbravo.ddlutils.util.DBSMOBUtil;

/** Reads database model and source data from files and creates a clean database with them. */
public class DBCreator {
  private static final Logger log = LogManager.getLogger();

  private File prescript = null;
  private File postscript = null;

  private File model;
  private boolean dropfirst = false;
  private boolean failonerror = false;

  private String object = null;

  private String basedir;
  private String modulesDir;

  private String dirFilter;
  private String input;

  private Platform platform;

  private static final String MSG_ERROR = "There were serious problems while creating the database. Please review and fix them before continuing with the creation of the database.";

  private List<Change> configScriptChanges;

  private boolean shouldWriteChecksumInfo;

  public Database createDatabase() {
    try {
      preCreateModel();
      Database db = createModel();
      postCreateModel(db);
      return db;
    } catch (final Exception e) {
      log.error("Error creating database", e);
      throw new BuildException(e);
    }
  }

  protected void preCreateModel() throws IOException {
    executePrescripts();
  }

  protected void postCreateModel(Database db) throws Exception {
    executePostscript();
    if (shouldWriteChecksumInfo) {
      writeChecksumInfo();
    }
    insertSourceData(db);
    enableDisabledObjects(db);
  }

  private void writeChecksumInfo() {
    log.info("Writing checksum info");
    DBSMOBUtil
        .writeCheckSumInfo(new File(model.getAbsolutePath() + "/../../../").getAbsolutePath());
  }

  private Database createModel() throws Exception {
    Database db = null;
    if (modulesDir == null) {
      log.info("modulesDir for additional files not specified. Creating database with just Core.");
      db = DatabaseUtils.readDatabaseWithoutConfigScript(getModel());
    } else {
      // We read model files using the filter, obtaining a file array. The models will be merged
      // to create a final target model.
      final Vector<File> dirs = new Vector<>();
      if (model.exists()) {
        dirs.add(model);
      }
      final DirectoryScanner dirScanner = new DirectoryScanner();
      dirScanner.setBasedir(new File(modulesDir));
      final String[] dirFilterA = { dirFilter };
      dirScanner.setIncludes(dirFilterA);
      dirScanner.scan();
      final String[] incDirs = dirScanner.getIncludedDirectories();
      for (int j = 0; j < incDirs.length; j++) {
        final File dirF = new File(modulesDir, incDirs[j]);
        dirs.add(dirF);
      }
      final File[] fileArray = new File[dirs.size()];
      for (int i = 0; i < dirs.size(); i++) {
        fileArray[i] = dirs.get(i);
      }

      db = DatabaseUtils.readDatabaseWithoutConfigScript(fileArray);
    }

    applyConfigScriptModelChanges(db);
    // Create database
    log.info("Executing creation script");
    // crop database if needed
    if (object != null) {
      final Database empty = new Database();
      empty.setName("empty");
      db = DatabaseUtils.cropDatabase(empty, db, object);
      log.info("for database object {}", object);
    } else {
      log.info("for the complete database");
    }

    if (!platform.createTables(db, isDropfirst(), !isFailonerror())) {
      throw new Exception(MSG_ERROR);
    }
    return db;
  }

  private void executePrescripts() throws IOException {
    executeSystemPreScript();
    executePreScript();
  }

  private void executePostscript() throws IOException {
    // execute the post-script
    if (getPostscript() == null) {
      // try to execute the default prescript
      final File fpost = new File(getModel(), "postscript-" + platform.getName() + ".sql");
      if (fpost.exists()) {
        log.info("Executing default postscript");
        platform.evaluateBatch(DatabaseUtils.readFile(fpost), !isFailonerror());
      }
    } else {
      platform.evaluateBatch(DatabaseUtils.readFile(getPostscript()), !isFailonerror());
    }
  }

  private void executeSystemPreScript() throws IOException {
    File script = new File(getModel(), "prescript-systemuser-" + platform.getName() + ".sql");
    if (script.exists()) {
      log.info("Executing script {}", script.getName());
      platform.evaluateBatchWithSystemUser(DatabaseUtils.readFile(script), !isFailonerror());
    }
  }

  private void executePreScript() throws IOException {
    File preScript = getPrescript();
    if (preScript == null) {
      // try to execute the default prescript
      preScript = new File(getModel(), "prescript-" + platform.getName() + ".sql");
    }
    if (preScript.exists()) {
      log.info("Executing script {}", preScript.getName());
      platform.evaluateBatch(DatabaseUtils.readFile(preScript), !isFailonerror());
    }
  }

  private void insertSourceData(Database db) throws Exception {
    // Now we insert sourcedata into the database first we load the data files
    final String folders = getInput();
    if (folders == null) {
      log.info("No source data dir, skipping source data");
      return;
    }
    final StringTokenizer strTokFol = new StringTokenizer(folders, ",");
    final Vector<File> files = new Vector<>();
    while (strTokFol.hasMoreElements()) {
      if (basedir == null) {
        log.info("Basedir not specified, will insert just Core data files.");
        final String folder = strTokFol.nextToken();
        final File[] fileArray = DatabaseUtils.readFileArray(new File(folder));
        for (int i = 0; i < fileArray.length; i++) {
          files.add(fileArray[i]);
        }
      } else {
        final String token = strTokFol.nextToken();
        final DirectoryScanner dirScanner = new DirectoryScanner();
        dirScanner.setBasedir(new File(basedir));
        final String[] dirFilterA = { token };
        dirScanner.setIncludes(dirFilterA);
        dirScanner.scan();
        final String[] incDirs = dirScanner.getIncludedDirectories();
        for (int j = 0; j < incDirs.length; j++) {
          final File dirFolder = new File(basedir, incDirs[j] + "/");
          final File[] fileArray = DatabaseUtils.readFileArray(dirFolder);
          for (int i = 0; i < fileArray.length; i++) {
            files.add(fileArray[i]);
          }
        }
      }
    }

    log.info("Disabling triggers");
    Connection connection = platform.borrowConnection();
    platform.disableAllTriggers(connection, db, false);
    platform.returnConnection(connection);

    log.info("Inserting data into the database.");
    // Now we insert the data into the database
    final DatabaseDataIO dbdio = new DatabaseDataIO();
    dbdio.setEnsureFKOrder(false);
    dbdio.setUseBatchMode(true);
    DataReader dataReader = dbdio.getConfiguredDataReader(platform, db);
    dataReader.getSink().start();

    for (int i = 0; i < files.size(); i++) {
      log.debug("Importing data from file: {}", files.get(i).getName());
      dbdio.writeDataToDatabase(dataReader, files.get(i));
    }

    /*
     * end method does final inserts when using batching, so call it after importing data files but
     * before doing anything using those data (template, fk's, not null, ... )
     */
    dataReader.getSink().end();

    applyConfigScriptDataChanges(db);

    log.info("Executing onCreateDefault statements");
    platform.executeOnCreateDefaultForMandatoryColumns(db, null);

  }

  private void enableDisabledObjects(Database db) throws Exception {
    log.info("Enabling notnull constraints");
    platform.enableNOTNULLColumns(db);

    boolean continueOnError = false;
    log.info("Creating foreign keys");
    boolean fksEnabled = platform.createAllFKs(db, continueOnError);

    Connection connection = platform.borrowConnection();
    log.info("Enabling triggers");
    boolean triggersEnabled = platform.enableAllTriggers(connection, db, false);
    platform.returnConnection(connection);

    executePostscript();

    if (!triggersEnabled) {
      log.error(
          "Not all the triggers were correctly activated. The most likely cause of this is that the XML file of the trigger is not correct.");
    }
    if (!fksEnabled) {
      log.error(
          "Not all the foreign keys were correctly activated. Please review which ones were not, and fix the missing references.");
    }
    if (!triggersEnabled || !fksEnabled) {
      throw new Exception(MSG_ERROR);
    }

  }

  private void applyConfigScriptModelChanges(Database db) {
    List<ModelChange> modelChanges = getConfigScriptChanges(db, ModelChange.class);

    log.info("Applying {} config script model changes...", modelChanges.size());

    for (ModelChange change : modelChanges) {
      log.debug("Applying config script model change {}", change);
      change.apply(db, platform.isDelimitedIdentifierModeOn());
    }
  }

  private void applyConfigScriptDataChanges(Database db) {
    List<DataChange> dataChanges = getConfigScriptChanges(db, DataChange.class);

    log.info("Applying {} config script data changes...", dataChanges.size());

    Vector<Change> changes = new Vector<>(dataChanges);
    platform.applyConfigScript(db, changes);
  }

  private <T extends Change> List<T> getConfigScriptChanges(Database db, Class<T> type) {
    if (configScriptChanges == null) {
      DatabaseData dbData = new DatabaseData(db);
      DatabaseUtils.readDataModuleInfo(db, dbData, basedir);

      configScriptChanges = new ArrayList<>();
      for (String template : DBSMOBUtil.getInstance().getSortedTemplates(dbData)) {
        File configScript = new File(new File(modulesDir),
            template + "/src-db/database/configScript.xml");
        log.info("Loading config script for module {}", template);
        if (configScript.exists()) {
          final DatabaseIO dbIO = new DatabaseIO();
          configScriptChanges.addAll(dbIO.readChanges(configScript));
        } else {
          log.error("Error. We couldn't find configuration script for template {}. Path: {}",
              configScript.getName(), configScript.getAbsolutePath());
        }
      }
    }

    return configScriptChanges.stream()
        .filter(type::isInstance)
        .map(type::cast)
        .collect(Collectors.toList());
  }

  public File getModel() {
    return model;
  }

  public DBCreator setModel(File model) {
    this.model = model;
    return this;
  }

  public DBCreator setBasedir(String basedir) {
    this.basedir = basedir;
    return this;
  }

  public DBCreator setDirFilter(String dirFilter) {
    this.dirFilter = dirFilter;
    return this;
  }

  public boolean isDropfirst() {
    return dropfirst;
  }

  public DBCreator setDropfirst(boolean dropfirst) {
    this.dropfirst = dropfirst;
    return this;
  }

  public boolean isFailonerror() {
    return failonerror;
  }

  public DBCreator setFailonerror(boolean failonerror) {
    this.failonerror = failonerror;
    return this;
  }

  public File getPrescript() {
    return prescript;
  }

  public DBCreator setPrescript(File prescript) {
    this.prescript = prescript;
    return this;
  }

  public File getPostscript() {
    return postscript;
  }

  public DBCreator setPostscript(File postscript) {
    this.postscript = postscript;
    return this;
  }

  public DBCreator setObject(String object) {
    if (object == null || object.trim().startsWith("$") || object.trim().equals("")) {
      this.object = null;
    } else {
      this.object = object;
    }
    return this;
  }

  public String getInput() {
    return input;
  }

  public DBCreator setInput(String input) {
    this.input = input;
    return this;
  }

  public DBCreator setModulesDir(String modulesDir) {
    this.modulesDir = modulesDir;
    return this;
  }

  public DBCreator setPlatform(Platform platform) {
    this.platform = platform;
    return this;
  }

  public Platform getPlatform() {
    return this.platform;
  }

  /**
   * Allows to define the configScript changes, if this is set to a non null value, they will not be
   * tried to be obtained from sources.
   */
  public DBCreator setConfigScriptChanges(List<Change> configScriptChanges) {
    this.configScriptChanges = configScriptChanges;
    return this;
  }

  public DBCreator setShouldWriteChecksumInfo(boolean shouldWriteChecksumInfo) {
    this.shouldWriteChecksumInfo = shouldWriteChecksumInfo;
    return this;
  }
}
