/*
 ************************************************************************************
 * Copyright (C) 2022 Openbravo S.L.U.
 * Licensed under the Apache Software License version 2.0
 * You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to  in writing,  software  distributed
 * under the License is distributed  on  an  "AS IS"  BASIS,  WITHOUT  WARRANTIES  OR
 * CONDITIONS OF ANY KIND, either  express  or  implied.  See  the  License  for  the
 * specific language governing permissions and limitations under the License.
 ************************************************************************************
 */
package org.openbravo.dbsm.test.base;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.core.Appender;
import org.apache.logging.log4j.core.Core;
import org.apache.logging.log4j.core.Filter;
import org.apache.logging.log4j.core.LogEvent;
import org.apache.logging.log4j.core.appender.AbstractAppender;
import org.apache.logging.log4j.core.config.plugins.Plugin;
import org.apache.logging.log4j.core.config.plugins.PluginAttribute;
import org.apache.logging.log4j.core.config.plugins.PluginElement;
import org.apache.logging.log4j.core.config.plugins.PluginFactory;

@Plugin(name = "TestLogAppender", category = Core.CATEGORY_NAME, elementType = Appender.ELEMENT_TYPE)
public class TestLogAppender extends AbstractAppender {
  private static List<String> msgs = new ArrayList<>();

  protected TestLogAppender(String name, Filter filter) {
    super(name, filter, null);
  }

  @PluginFactory
  public static TestLogAppender createAppender(@PluginAttribute("name") String name,
      @PluginElement("Filter") Filter filter) {
    return new TestLogAppender(name, filter);
  }

  @Override
  public void append(LogEvent logEvent) {
    if (logEvent.getLevel().isMoreSpecificThan(Level.WARN)) {
      msgs.add(logEvent.getLevel() + ": " + logEvent.getMessage());
    }
  }

  public static List<String> getWarnAndErrors() {
    return msgs;
  }

  public static void reset() {
    msgs = new ArrayList<>();
  }

}
