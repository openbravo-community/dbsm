/*
 ************************************************************************************
 * Copyright (C) 2021-2022 Openbravo S.L.U.
 * Licensed under the Apache Software License version 2.0
 * You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to  in writing,  software  distributed
 * under the License is distributed  on  an  "AS IS"  BASIS,  WITHOUT  WARRANTIES  OR
 * CONDITIONS OF ANY KIND, either  express  or  implied.  See  the  License  for  the
 * specific language governing permissions and limitations under the License.
 ************************************************************************************
 */

package org.openbravo.dbsm.test.sampledata;

import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorCompletionService;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.apache.ddlutils.model.Database;
import org.junit.Before;
import org.junit.Test;
import org.openbravo.dbsm.test.base.DbsmTest;
import org.openbravo.ddlutils.task.ImportSampledata;
import org.openbravo.ddlutils.task.ImportSampledata.ImportRunner;
import org.openbravo.test.base.Issue;

public class ImportSampleData extends DbsmTest {

  private Database db;

  public ImportSampleData(String rdbms, String driver, String url, String sid, String user,
      String password, String name) throws IOException {
    super(rdbms, driver, url, sid, user, password, name);
  }

  @Before
  public void createDB() {
    db = createDatabase("sampledata/SAMPLE_DATA_MODEL.xml");
  }

  @Test
  public void insertsSampleData() throws InterruptedException, ExecutionException, SQLException {
    importSampleData("SIMPLE.xml");
    Row sampleDataRow = getRowValues("TEST", "1");
    assertRowValues(sampleDataRow, Map.of(
    //@formatter:off
        "std", "S", 
        "req1", "R1", 
        "req2", "R2", 
        "req3", "R3"
    //@formatter:on
    ));
  }

  @Test
  public void emptyColumnsAreNull() throws InterruptedException, ExecutionException, SQLException {
    importSampleData("OPT_NULL.xml");
    Row sampleDataRow = getRowValues("TEST", "1");
    assertRowValues(sampleDataRow, Map.of(
    //@formatter:off
        "std", Optional.empty(), 
        "req1", "R1", 
        "req2", "R2", 
        "req3", "R3"
    //@formatter:on
    ));
  }

  @Test
  public void unknownColumnsAreIgnored()
      throws InterruptedException, ExecutionException, SQLException {
    importSampleData("UNKNOWN_COL.xml");
    Row sampleDataRow = getRowValues("TEST", "1");
    assertRowValues(sampleDataRow, Map.of(
    //@formatter:off
        "std", Optional.empty(), 
        "req1", "R1", 
        "req2", "R2", 
        "req3", "R3"
    //@formatter:on
    ));
  }

  @Test
  public void literalCreateDefaultIsApplied()
      throws InterruptedException, ExecutionException, SQLException {
    importSampleData("NO_REQ1.xml");
    Row sampleDataRow = getRowValues("TEST", "1");
    assertRowValues(sampleDataRow, Map.of(
    //@formatter:off
        "req1", "CD"
    //@formatter:on
    ));
  }

  @Test
  public void defaultIsApplied() throws InterruptedException, ExecutionException, SQLException {
    importSampleData("NO_REQ2.xml");
    Row sampleDataRow = getRowValues("TEST", "1");
    assertRowValues(sampleDataRow, Map.of(
    //@formatter:off
        "req2", "D"
    //@formatter:on
    ));
  }

  @Test
  public void createDefaultPrecedesDefault()
      throws InterruptedException, ExecutionException, SQLException {
    importSampleData("NO_REQ3.xml");
    Row sampleDataRow = getRowValues("TEST", "1");
    assertRowValues(sampleDataRow, Map.of(
    //@formatter:off
        "req3", "CD"
    //@formatter:on
    ));
  }

  @Test
  @Issue("49897")
  public void createDefaultReferencingAColumn()
      throws InterruptedException, ExecutionException, SQLException {
    db = createDatabase("sampledata/SAMPLE_DATA_MODEL2.xml");

    importSampleData("REFERENCE_CREATEDEFAULT.xml");

    Row sampleDataRow = getRowValues("TEST", "1");
    assertRowValues(sampleDataRow, Map.of(
    //@formatter:off
        "req1", "Std Value"
    //@formatter:on
    ));
  }

  private void importSampleData(String sampleDataFile)
      throws InterruptedException, ExecutionException {
    File dbModel = new File("data/sampledata", sampleDataFile);
    ImportRunner ir = new ImportSampledata.ImportRunner(getPlatform(), db, dbModel,
        getRdbms().toString());

    ExecutorService es = Executors.newFixedThreadPool(1);
    ExecutorCompletionService<Void> ecs = new ExecutorCompletionService<>(es);
    ecs.submit(ir);
    ecs.take().get();

  }

}
