/*
 ************************************************************************************
 * Copyright (C) 2022 Openbravo S.L.U.
 * Licensed under the Apache Software License version 2.0
 * You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to  in writing,  software  distributed
 * under the License is distributed  on  an  "AS IS"  BASIS,  WITHOUT  WARRANTIES  OR
 * CONDITIONS OF ANY KIND, either  express  or  implied.  See  the  License  for  the
 * specific language governing permissions and limitations under the License.
 ************************************************************************************
 */
package org.openbravo.dbsm.test.model.recreation;

import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.anyOf;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.not;

import java.io.IOException;
import java.util.List;

import org.junit.Test;
import org.openbravo.dbsm.test.base.DbsmTest;

public class MaterializedView extends DbsmTest {

  public MaterializedView(String rdbms, String driver, String url, String sid, String user,
      String password, String name) throws IOException {
    super(rdbms, driver, url, sid, user, password, name);
  }

  @Test
  public void shouldNotBeRecreatedIfDependsOnNotModifiedTable() {
    createDatabase("materializedViews/BASE_MODEL.xml");
    List<String> updateStatements = sqlStatmentsForUpdate("materializedViews/BASE_MODEL.xml");

    assertThat("changes between updated db and target db", updateStatements,
        not(anyOf(hasItem(containsString("DROP MATERIALIZED VIEW")),
            hasItem(containsString("CREATE MATERIALIZED")))));
  }

  @Test
  public void shouldBeRecreatedIfItDependsOnView() {
    createDatabase("materializedViews/BASE_MODEL_WITH_VIEW_DEPENDENCY.xml");
    List<String> updateStatements = sqlStatmentsForUpdate(
        "materializedViews/BASE_MODEL_WITH_VIEW_DEPENDENCY.xml");

    assertThat("changes between updated db and target db", updateStatements,
        allOf(hasItem(containsString("DROP MATERIALIZED VIEW")),
            hasItem(containsString("CREATE MATERIALIZED"))));
  }

  @Test
  public void shouldBeRecreatedIfItDependsOnMaterializedView() {
    createDatabase("materializedViews/BASE_MODEL_WITH_MATERIALIZED_VIEW_DEPENDENCY.xml");
    List<String> updateStatements = sqlStatmentsForUpdate(
        "materializedViews/BASE_MODEL_WITH_MATERIALIZED_VIEW_DEPENDENCY.xml");

    assertThat("changes between updated db and target db", updateStatements,
        allOf(hasItem(containsString("DROP MATERIALIZED VIEW")),
            hasItem(containsString("CREATE MATERIALIZED"))));
  }

  @Test
  public void shouldBeRecreatedIfItDependsOnUpdatedTable() {
    createDatabase("materializedViews/BASE_MODEL.xml");

    List<String> updateStatements = sqlStatmentsForUpdate(
        "materializedViews/BASE_MODEL_UPDATED_BASE_TABLE.xml");

    assertThat("changes between updated db and target db", updateStatements,
        allOf(hasItem(containsString("DROP MATERIALIZED VIEW")),
            hasItem(containsString("CREATE MATERIALIZED"))));
  }

  @Test
  public void shouldBeRecreatedIfItDependsOnRemovedFunction() {
    createDatabase("materializedViews/BASE_MODEL_WITH_FUNCTION_DEPENDENCY.xml");

    List<String> updateStatements = sqlStatmentsForUpdate(
        "materializedViews/BASE_MODEL_WITH_FUNCTION_DEPENDENCY_UPDATED.xml");

    assertThat("changes between updated db and target db", updateStatements,
        allOf(hasItem(containsString("DROP MATERIALIZED VIEW")),
            hasItem(containsString("CREATE MATERIALIZED"))));
  }

  @Test
  public void shouldBeRecreatedIfItIsUpdated() {
    createDatabase("materializedViews/BASE_MODEL.xml");

    List<String> updateStatements = sqlStatmentsForUpdate(
        "materializedViews/BASE_MODEL_UPDATED_MATERIALIZED_VIEW.xml");

    assertThat("changes between updated db and target db", updateStatements,
        allOf(hasItem(containsString("DROP MATERIALIZED VIEW")),
            hasItem(containsString("CREATE MATERIALIZED"))));
  }

}
